﻿unit editreport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, Vcl.StdCtrls, Vcl.DBCtrls, DBCtrlsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, DBSQLLookUp, Vcl.Mask, Vcl.Buttons,
  PngSpeedButton, Vcl.ComCtrls, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls,
  MemTableDataEh, MemTableEh, DataDriverEh, DBXDataDriverEh, ADODataDriverEh, functions,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFormEditReport = class(TFormEdit)
    pcAll: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    btAdd: TPngSpeedButton;
    btDelete: TPngSpeedButton;
    sbRowUp: TSpeedButton;
    sbRowDown: TSpeedButton;
    dpFields: TDBGridEh;
    TabSheet2: TTabSheet;
    plTop: TPanel;
    sbRefreshParams: TPngSpeedButton;
    dgParameters: TDBGridEh;
    Label5: TLabel;
    sbSQLFull: TSpeedButton;
    meSQL: TDBMemo;
    dsFields: TDataSource;
    foDialog: TFileOpenDialog;
    dsParams: TDataSource;
    sbEditParam: TPngSpeedButton;
    sbDeleteParam: TPngSpeedButton;
    sbRefreshFields: TPngSpeedButton;
    Panel2: TPanel;
    edGroupFields: TDBEditEh;
    edName: TDBEditEh;
    edTemplate: TDBEditEh;
    drvFields: TFDTableAdapter;
    meFields: TFDMemTable;
    drvParams: TFDTableAdapter;
    meParams: TFDMemTable;
    cmdFieldsSelect: TFDCommand;
    cmdParamsSelect: TFDCommand;
    Label2: TLabel;
    sbPrepareFull: TSpeedButton;
    mePrepare: TDBMemo;
    cmdParamFetch: TFDCommand;
    procedure btAddClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pcAllChange(Sender: TObject);
    procedure meFieldsAfterInsert(DataSet: TDataSet);
    procedure sbSQLFullClick(Sender: TObject);
    procedure sbEditParamClick(Sender: TObject);
    procedure meParamsAfterInsert(DataSet: TDataSet);
    procedure sbRefreshParamsClick(Sender: TObject);
    procedure sbDeleteParamClick(Sender: TObject);
    procedure dpFieldsKeyPress(Sender: TObject; var Key: Char);
    procedure dgParametersKeyPress(Sender: TObject; var Key: Char);
    procedure sbRefreshFieldsClick(Sender: TObject);
    procedure meFieldsAfterPost(DataSet: TDataSet);
    procedure meParamsAfterPost(DataSet: TDataSet);
    procedure btnOkClick(Sender: TObject);
    procedure edTemplateEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure sbRowUpClick(Sender: TObject);
    procedure sbRowDownClick(Sender: TObject);
    procedure sbPrepareFullClick(Sender: TObject);
  private
    procedure ActivateCurrentRecord(ds: TFDMemTable; drv: TFDTableAdapter);
  public
    { Public declarations }
  end;

var
  FormEditReport: TFormEditReport;

implementation

{$R *.dfm}

uses reports, editsql, editreportparam, ReportItem, Catalogs, dmu,
  selecttemplate;

procedure TFormEditReport.btAddClick(Sender: TObject);
begin
  meFields.Append;
end;

procedure TFormEditReport.btDeleteClick(Sender: TObject);
begin
  meFields.Delete;
end;

procedure TFormEditReport.btnOkClick(Sender: TObject);
begin
  inherited;
  if (meFields.State in [dsEdit, dsInsert]) then meFields.Post;
  if (meParams.State in [dsEdit, dsInsert]) then meParams.Post;
end;

procedure TFormEditReport.dgParametersKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin

    if dsEdit = meParams.State then
      meParams.Post;

    if dsInsert = meParams.State then
      meParams.Post;

  end;
end;

procedure TFormEditReport.dpFieldsKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin

    if dsEdit = meFields.State then
      meFields.Post;

    if dsInsert = meFields.State then
      meFields.Post;

  end;
end;

procedure TFormEditReport.edTemplateEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  FormSelectTemplate.Init;
  FormSelectTemplate.ShowModal;
  if FormSelectTemplate.ModalResult = mrOk then
  begin
    edTemplate.Text :=  FormSelectTemplate.meFiles.FieldByName('template_name').AsString;
  end;

end;

procedure TFormEditReport.FormShow(Sender: TObject);
begin

  inherited;

  drvFields.SelectCommand.ParamByName('report_id').Value := current_id;
  meFields.Close;
  meFields.Open;

  drvParams.SelectCommand.ParamByName('report_id').Value := current_id;
  meParams.Close;
  meParams.Open;

end;

procedure TFormEditReport.meFieldsAfterInsert(DataSet: TDataSet);
begin

  if current_id = 0 then
  begin
    dm.meData.Post;
    current_id := dm.meData.FieldByName('rowid').AsInteger;
  end;
  meFields.FieldByName('report_id').Value := current_id;

end;

procedure TFormEditReport.meFieldsAfterPost(DataSet: TDataSet);
begin
  ActivateCurrentRecord(meFields, drvFields);
end;

procedure TFormEditReport.meParamsAfterInsert(DataSet: TDataSet);
begin
  if current_id = 0 then
  begin
    dm.meData.Post;
    current_id := dm.meData.FieldByName('rowid').AsInteger;
  end;
  meParams.FieldByName('report_id').Value := current_id;
end;

procedure TFormEditReport.meParamsAfterPost(DataSet: TDataSet);
begin
  ActivateCurrentRecord(meParams, drvParams);
end;

procedure TFormEditReport.pcAllChange(Sender: TObject);
begin
  if current_id = 0 then
  begin
    dm.meData.Post;
    current_id := dm.meData.FieldByName('rowid').AsInteger;
  end;
end;

procedure TFormEditReport.sbDeleteParamClick(Sender: TObject);
begin
  meParams.Delete;
end;

procedure TFormEditReport.sbEditParamClick(Sender: TObject);
begin
  EE(nil, FormEditReportParam, meParams, 'report_params');
end;

procedure TFormEditReport.sbPrepareFullClick(Sender: TObject);
begin
  FormEditSQL.meSQL.Text := self.mePrepare.Text;
  FormEditSQL.ShowModal;
  if FormEditSQL.ModalResult = mrOk then
  begin
    self.mePrepare.Text := FormEditSQL.meSQL.Text;
  end;
end;

procedure TFormEditReport.sbRefreshFieldsClick(Sender: TObject);
var i: integer;
begin
  if meFields.RecordCount = 0 then
  begin
    if not FormReportItem.meReport.Active then exit;
    for i := 0 to FormReportItem.meReport.FieldCount-1 do
    begin
      meFields.Append;
      meFields.FieldByName('field_name').Value := FormReportItem.meReport.Fields[i].FieldName;
      meFields.Post;
    end;
    meFields.Close;
    meFields.Open;
  end;
end;

procedure TFormEditReport.sbRefreshParamsClick(Sender: TObject);
var i: integer;
begin

  dsLocal.DataSet.Post;
  dsLocal.DataSet.Edit;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Text := self.mePrepare.Lines.Text;
  for i := 0 to qrAux.ParamCount-1 do
  begin
    meParams.First;
    if not meParams.Locate('param_name', qrAux.Params[i].Name,[]) then
    begin
      meParams.Append;
      meParams.FieldByName('param_name').Value := qrAux.Params[i].Name;
      meParams.Post;
    end;
  end;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Text := self.meSQL.Lines.Text;
  meParams.DisableControls;
  for i := 0 to qrAux.ParamCount-1 do
  begin
    meParams.First;
    if not meParams.Locate('param_name', qrAux.Params[i].Name,[]) then
    begin
      meParams.Append;
      meParams.FieldByName('param_name').Value := qrAux.Params[i].Name;
      meParams.Post;
    end;
  end;
  drvParams.SelectCommand.ParamByName('report_id').Value := current_id;
  meParams.Close;
  meParams.Open;
  meParams.FetchAll;
  meParams.First;
  dgParameters.Refresh;
end;

procedure TFormEditReport.sbRowDownClick(Sender: TObject);
var id: integer;
begin

  id := meFields.FieldByName('id').AsInteger;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('insert or replace into report_fields');
  qrAux.SQL.Add('select id, report_id, field_name, field_caption,');
  qrAux.SQL.Add('(ROW_NUMBER() OVER(ORDER BY field_order, id))*10 AS field_order,');
  qrAux.SQL.Add('field_width, field_align, is_total');
  qrAux.SQL.Add('from report_fields');
  qrAux.SQL.Add('where report_id ='+IntToStr(self.current_id));
  qrAux.ExecSQL;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update report_fields set field_order = field_order + 15');
  qrAux.SQL.Add('where id = '+ meFields.FieldByName('id').AsString);
  qrAux.ExecSQL;

  meFields.Close;
  meFields.Open;

  meFields.Locate('id', id, []);


end;

procedure TFormEditReport.sbRowUpClick(Sender: TObject);
var id: integer;
begin

  id := meFields.FieldByName('id').AsInteger;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('insert or replace into report_fields');
  qrAux.SQL.Add('select id, report_id, field_name, field_caption,');
  qrAux.SQL.Add('(ROW_NUMBER() OVER(ORDER BY field_order, id))*10 AS field_order,');
  qrAux.SQL.Add('field_width, field_align, is_total');
  qrAux.SQL.Add('from report_fields');
  qrAux.SQL.Add('where report_id ='+IntToStr(self.current_id));
  qrAux.ExecSQL;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update report_fields set field_order = field_order - 15');
  qrAux.SQL.Add('where id = '+ meFields.FieldByName('id').AsString);
  qrAux.ExecSQL;

  meFields.Close;
  meFields.Open;

  meFields.Locate('id', id, []);

end;

procedure TFormEditReport.sbSQLFullClick(Sender: TObject);
begin
  FormEditSQL.meSQL.Text := self.meSQL.Text;
  FormEditSQL.ShowModal;
  if FormEditSQL.ModalResult = mrOk then
  begin
    self.meSQL.Text := FormEditSQL.meSQL.Text;
  end;
end;


procedure TFormEditReport.ActivateCurrentRecord(ds: TFDMemTable; drv: TFDTableAdapter);
var currentid: integer;
begin

  if ds.FieldByName('rowid').AsInteger>0 then exit;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select IDENT_CURRENT(''report_fields'')');
  qrAux.Open;
  currentid := qrAux.Fields[0].AsInteger;

  if currentid <> 0 then
  begin
    drv.FetchRowCommand.ParamByName('current_id').Value := currentid;
    ds.RefreshRecord;
  end else
  begin
    RefreshRecord(ds,currentid);
  end;

end;


end.
