﻿unit Catalogs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Stan.Async;

type
  TFormCatalogs = class(TFormGrid)
    procedure meDataBeforeOpen(DataSet: TDataSet);
  private
  public
    system_section: string;
    procedure Init; override;
  end;

var
  FormCatalogs: TFormCatalogs;

implementation

{$R *.dfm}

uses editcatalog, dmu;

procedure TFormCatalogs.Init;
begin
  self.formEdit := FormEditCatalog;
  FormEditCatalog.system_section := system_section;
  inherited;
  dm.meData.Close;
  dm.meData.Open;
end;

procedure TFormCatalogs.meDataBeforeOpen(DataSet: TDataSet);
begin
 dm.drvData.SelectCommand.ParamByName('folder_section0').Value := system_section;
 dm.drvData.InsertCommand.ParamByName('folder_section0').Value := system_section;
end;

end.
