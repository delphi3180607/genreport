﻿unit selecttemplate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Data.DB, Data.Win.ADODB, Vcl.StdCtrls, Vcl.ExtCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, IOUtils;

type
  TFormSelectTemplate = class(TFormEdit)
    dpTemplate: TDBGridEh;
    meFiles: TFDMemTable;
    meFilestemplate_name: TStringField;
  private
    { Private declarations }
  public
    procedure Init;
  end;

var
  FormSelectTemplate: TFormSelectTemplate;

implementation

{$R *.dfm}

procedure TFormSelectTemplate.Init;
var filename, filenameshort, path: string;
begin
   path := ExtractFilePath(Application.ExeName)+'templates';
   meFiles.Active := true;
   for filename in TDirectory.GetFiles(path)  do
   begin
     filenameshort := ExtractFileName(filename);
     meFiles.Append;
     meFiles.FieldByName('template_name').Value := filenameshort;
     meFiles.Post;
   end;
   meFiles.First;
end;

end.
