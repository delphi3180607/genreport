﻿program genreport;

uses
  Vcl.Forms,
  edit in 'edit.pas' {FormEdit},
  editreport in 'editreport.pas' {FormEditReport},
  editreportparam in 'editreportparam.pas' {FormEditReportParam},
  Functions in 'Functions.pas',
  grid in 'grid.pas' {FormGrid},
  ReportItem in 'ReportItem.pas' {FormReportItem},
  reports in 'reports.pas' {FormReports},
  Settings in 'Settings.pas' {FormSettings},
  dmu in 'dmu.pas' {dm: TDataModule},
  QYN in 'QYN.pas' {FormQYN},
  BigMessage in 'BigMessage.pas' {FormBigMessage},
  editsql in 'editsql.pas' {FormEditSql},
  ParamsFormUnit in 'ParamsFormUnit.pas' {ParamsForm},
  selecttemplate in 'selecttemplate.pas' {FormSelectTemplate},
  Catalogs in 'Catalogs.pas' {FormCatalogs},
  editcatalog in 'editcatalog.pas' {FormEditCatalog},
  Vcl.Themes,
  Vcl.Styles,
  main in 'main.pas' {FormMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Win10IDE_Light');
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormReports, FormReports);
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TFormEdit, FormEdit);
  Application.CreateForm(TFormEditReport, FormEditReport);
  Application.CreateForm(TFormEditReportParam, FormEditReportParam);
  Application.CreateForm(TFormGrid, FormGrid);
  Application.CreateForm(TFormReportItem, FormReportItem);
  Application.CreateForm(TFormSettings, FormSettings);
  Application.CreateForm(TFormQYN, FormQYN);
  Application.CreateForm(TFormBigMessage, FormBigMessage);
  Application.CreateForm(TFormEditSql, FormEditSql);
  Application.CreateForm(TParamsForm, ParamsForm);
  Application.CreateForm(TFormSelectTemplate, FormSelectTemplate);
  Application.CreateForm(TFormCatalogs, FormCatalogs);
  Application.CreateForm(TFormEditCatalog, FormEditCatalog);
  FormMain.Init;
  Application.Run;
end.
