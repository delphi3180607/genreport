﻿unit Settings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl, Data.DB, Vcl.Mask, DBCtrlsEh;

type
  TFormSettings = class(TForm)
    Panel22: TPanel;
    dsLocal: TDataSource;
    edServer: TDBEditEh;
    edDbName: TDBEditEh;
    edLogin: TDBEditEh;
    edPassword: TDBEditEh;
    btnOk: TButton;
    btnCancel: TButton;
    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
  private
  public
  end;

var
  FormSettings: TFormSettings;

implementation

{$R *.dfm}

procedure TFormSettings.btOkClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFormSettings.btCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
