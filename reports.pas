﻿unit reports;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB, EhLibMTE,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.StdCtrls, DBXDataDriverEh, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Vcl.Mask, DBCtrlsEh, DBSQLLookUp, functions, EXLReportDictionary,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.ComCtrls, DBLookupEh;

type
  TFormReports = class(TFormGrid)
    mm: TMainMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    procedure N9Click(Sender: TObject);
  private
    procedure EditConnectionSettings;
  public
    procedure Init; override;
  end;

var
  FormReports: TFormReports;

implementation

{$R *.dfm}

uses editreport, ParamsFormUnit, dmu, selecttemplate, Settings;

procedure TFormReports.Init;
begin
  dm.SetConnection;
  inherited;
  self.formEdit := FormEditReport;
end;

procedure TFormReports.N9Click(Sender: TObject);
begin
  EditConnectionSettings;
end;

procedure TFormReports.EditConnectionSettings;
begin
  FormSettings.dsLocal.DataSet := dm.tblConnection;
  dm.tblConnection.Edit;
  FormSettings.ShowModal;
  if FormSettings.ModalResult = mrOK then
  begin
    dm.tblConnection.Post;
    Init;
  end else
  begin
    dm.tblConnection.Cancel;
  end;
end;

end.
