﻿inherited FormReports: TFormReports
  AlignWithMargins = True
  BorderWidth = 3
  Caption = #1054#1090#1095#1077#1090#1099
  ClientHeight = 570
  ClientWidth = 769
  Menu = mm
  ExplicitWidth = 791
  ExplicitHeight = 634
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 529
    Width = 769
    ExplicitTop = 529
    ExplicitWidth = 769
    inherited btnOk: TButton
      Left = 534
      ExplicitLeft = 534
    end
    inherited btnCancel: TButton
      Left = 653
      ExplicitLeft = 653
    end
  end
  inherited plAll: TPanel
    Width = 769
    Height = 529
    Caption = ''
    Color = clWindow
    ExplicitWidth = 769
    ExplicitHeight = 529
    inherited plTop: TPanel
      Width = 769
      Height = 33
      ExplicitWidth = 769
      ExplicitHeight = 33
      inherited sbDelete: TPngSpeedButton
        Left = 28
        Top = 1
        Width = 32
        Height = 32
        ExplicitLeft = 0
        ExplicitTop = 29
        ExplicitWidth = 32
        ExplicitHeight = 781
      end
      inherited btFilter: TPngSpeedButton
        Left = 130
        Top = 1
        Width = 44
        Height = 32
        Visible = False
        ExplicitLeft = 0
        ExplicitTop = 131
        ExplicitWidth = 44
        ExplicitHeight = 781
      end
      inherited btExcel: TPngSpeedButton
        Left = 86
        Top = 1
        Width = 44
        Height = 32
        Visible = False
        ExplicitLeft = 0
        ExplicitTop = 87
        ExplicitWidth = 44
        ExplicitHeight = 781
      end
      inherited sbAdd: TPngSpeedButton
        Top = 1
        Width = 28
        Height = 32
        Flat = True
        ExplicitTop = 1
        ExplicitWidth = 28
        ExplicitHeight = 781
      end
      inherited sbEdit: TPngSpeedButton
        Left = 60
        Top = 1
        Width = 26
        Height = 32
        ExplicitLeft = 0
        ExplicitTop = 61
        ExplicitWidth = 26
        ExplicitHeight = 781
      end
      inherited Bevel2: TBevel
        Left = 3
        Width = 763
        Height = 1
        Align = alTop
        ExplicitLeft = 106
        ExplicitTop = 0
        ExplicitWidth = 265
        ExplicitHeight = 1
      end
      inherited plCount: TPanel
        Left = 575
        Top = 1
        Height = 32
        ExplicitLeft = 575
        ExplicitTop = 1
        ExplicitHeight = 32
        inherited edCount: TDBEditEh
          Left = 3
          Height = 26
          Align = alLeft
          ControlLabel.ExplicitLeft = -103
          ControlLabel.ExplicitTop = 9
          ControlLabel.ExplicitWidth = 103
          Visible = False
          ExplicitLeft = 3
          ExplicitHeight = 26
        end
      end
    end
    inherited dgData: TDBGridEh
      Top = 33
      Width = 769
      Height = 496
      AutoFitColWidths = True
      Border.Color = clSilver
      ColumnDefValues.EditButtonDrawBackTime = edbtNeverEh
      Ctl3D = True
      DataGrouping.Active = True
      DataGrouping.Color = 15592150
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.Font.Color = 9918500
      DataGrouping.GroupLevels = <
        item
        end>
      DataGrouping.ParentColor = False
      DataGrouping.ParentFont = False
      DrawMemoText = True
      Font.Style = [fsBold]
      GridLineParams.BrightColor = 15395041
      GridLineParams.DarkColor = 15395041
      GridLineParams.DataBoundaryColor = 15395041
      HorzScrollBar.Tracking = False
      HorzScrollBar.SmoothStep = False
      IndicatorParams.FillStyle = cfstGradientEh
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghData3D, dghHighlightFocus, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ParentColor = True
      RowHeight = 22
      STFilter.Visible = False
      TitleParams.FillStyle = cfstGradientEh
      TitleParams.MultiTitle = True
      VertScrollBar.Width = 4
      OnDrawColumnCell = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'report_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          TextEditing = False
          Title.Caption = #1054#1058#1063#1045#1058#1067
          Width = 382
          WordWrap = True
        end>
    end
    inherited plHint: TPanel
      Left = 51
      Width = 191
      ExplicitLeft = 51
      ExplicitWidth = 191
      inherited lbText: TLabel
        Width = 183
      end
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 152
    Top = 176
  end
  inherited dsData: TDataSource
    Left = 216
    Top = 176
  end
  inherited al: TActionList
    Left = 93
    Top = 178
  end
  inherited exReport: TEXLReport
    Left = 270
    Top = 176
  end
  object mm: TMainMenu
    Left = 648
    Top = 7
    object N8: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      object N9: TMenuItem
        Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103' '#1082' '#1041#1044
        OnClick = N9Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object N11: TMenuItem
        Action = aAdd
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1090#1095#1077#1090
      end
      object N12: TMenuItem
        Action = aEdit
        Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100' '#1086#1090#1095#1077#1090
      end
      object N13: TMenuItem
        Action = aDelete
        Caption = #1059#1076#1072#1083#1080#1090#1100' '#1086#1090#1095#1077#1090
      end
    end
  end
end
