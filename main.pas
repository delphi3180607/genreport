﻿unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEh, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, Vcl.StdCtrls,
  Vcl.Mask, DBCtrlsEh, DBLookupEh, Vcl.Buttons, PngSpeedButton, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, MemTableDataEh,
  EXLReportExcelTLB, EXLReportBand, EXLReport, EXLReportDictionary,
  MemTableEh, Data.Win.ADODB, FireDAC.Stan.Async, FireDAC.DApt, DBSQLLookUp,
  Vcl.Menus, System.Actions, Vcl.ActnList, JvActionsEngine, JvControlActions,
  CompoMansEh;

type
  TFormMain = class(TForm)
    plTool: TPanel;
    sbStart: TPngSpeedButton;
    btExport: TPngSpeedButton;
    sbClose: TPngSpeedButton;
    Label1: TLabel;
    dlReports: TDBLookupComboboxEh;
    dpReports: TDBGridEh;
    memSelectedReport: TFDMemTable;
    memSelectedReportrowid: TIntegerField;
    dsSelectedReport: TDataSource;
    cmdPrepareReport: TADOCommand;
    qrReport: TADOQuery;
    meReport: TMemTableEh;
    dsReport: TDataSource;
    exReport: TEXLReport;
    plMessage: TPanel;
    dsData: TDataSource;
    mm: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure sbCloseClick(Sender: TObject);
    procedure sbStartClick(Sender: TObject);
    procedure btExportClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    procedure FormatColumns;
    procedure GenerateColumns;
  public
    procedure Init;
    procedure GenerateReportData;
    procedure AssignParameters(dsParams: TDataSet);
    procedure FillParamsForm;
    function GotParams: boolean;
    function MakeControl(field_name: string; data_type: integer; data_length: integer; decimal_length: integer; dsParam: TDataSet): TControl;
    procedure MakeExcelReport;
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses dmu, ParamsFormUnit, Functions, reports;

procedure TFormMain.Init;
begin
  dm.connSettings.Params.Database := ExtractFilePath(Application.ExeName)+'settings.db';
  dm.connSettings.Open;
  dm.tblConnection.Open;
  if dm.tblConnection.RecordCount=0 then
  begin
    dm.tblConnection.Append;
    dm.tblConnection.Post;
  end;
  dm.meData.Open;
  memSelectedReport.Open;
  if memSelectedReport.RecordCount=0 then
  begin
    memSelectedReport.Append;
    memSelectedReport.Fields[0].Value := dm.meData.FieldByName('rowid').AsInteger;
    memSelectedReport.Post;
  end;
  dm.SetConnection;
end;

procedure TFormMain.sbCloseClick(Sender: TObject);
begin
  meReport.Close;
  dpReports.Columns.Clear;
end;

procedure TFormMain.sbStartClick(Sender: TObject);
begin
  GenerateReportData;
end;

procedure TFormMain.GenerateReportData;
begin

  qrReport.Close;
  meReport.Close;
  dpReports.Columns.Clear;
  meReport.Filter := '';
  dpReports.ClearFilter;
  dpReports.DataGrouping.GroupLevels.Clear;
  dpReports.DataGrouping.Footers.Clear;
  dpReports.DataGrouping.Active := false;
  dpReports.OddRowColor := $00F0F4F4;
  dpReports.GridLineParams.DataHorzColor := $00DEE2E2;
  dpReports.GridLineParams.DataVertColor := $00DEE2E2;
  FillParamsForm();
  if GotParams() then
  begin

    cmdPrepareReport.CommandText := dm.meData.FieldByName('prepare_sql').AsString;
    qrReport.SQL.Text := dm.meData.FieldByName('query_sql').AsString;

    AssignParameters(ParamsForm.meLocal);

    try
      GenerateColumns;
      plMessage.Show;
      Application.ProcessMessages;
      Screen.Cursor := crHourGlass;

      if trim(cmdPrepareReport.CommandText)<>'' then
      begin
        cmdPrepareReport.Execute;
      end;

      qrReport.Open;
      meReport.Close;
      meReport.LoadFromDataSet(qrReport, 0, lmCopy, false);
      FormatColumns;

      meReport.Open;

      dpReports.Show;
      btExport.Enabled := true;
      sbClose.Enabled := true;
    except
      on E : Exception do
      begin
        plMessage.Hide;
        Screen.Cursor := crDefault;
        ShowMessage('Ошибка при выполнении запроса: '+E.Message);
      end;
    end;

    plMessage.Hide;
    Screen.Cursor := crDefault;

  end;

end;

procedure TFormMain.GenerateColumns;
var c: TColumnEh;
begin

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select * from report_fields where report_id = :report_id order by field_order');
  dm.qrAux.ParamByName('report_id').Value := dm.meData.FieldByName('rowid').AsInteger;
  dm.qrAux.Open;

  dpReports.Columns.Clear;

  while not (dm.qrAux.Eof) do
  begin

    c := dpReports.Columns.Add;

    c.Title.Caption := dm.qrAux.FieldByName('field_caption').AsString;
    c.FieldName := dm.qrAux.FieldByName('field_name').AsString;
    c.Width := dm.qrAux.FieldByName('field_width').AsInteger;
    c.Title.TitleButton := true;
    c.TextEditing := true;

    if dm.qrAux.FieldByName('field_align').AsInteger>0 then
      c.Alignment := TAlignment(dm.qrAux.FieldByName('field_align').AsInteger - 1);

    dm.qrAux.Next;

  end;

end;


procedure TFormMain.FormatColumns;
var i: integer; c: TColumnEh;
begin

   for i := 0 to dpReports.Columns.Count-1 do
   begin

    c := dpReports.Columns[i];

    if c.Field = nil then
    begin
      ShowMessage('Ошибка определения полей отчета');
      exit;
    end;

    if c.Field.DataType = ftFloat then
    begin
      c.DisplayFormat := '### ### ##0.00';
    end;


   end;

end;

procedure TFormMain.btExportClick(Sender: TObject);
begin
  MakeExcelReport;
end;

procedure TFormMain.FillParamsForm;
var pl: TPanel; l: TLabel; c: TControl; sumHeight: integer;
begin

  ParamsForm.meLocal.Close;
  ParamsForm.meLocal.FieldDefs.Clear;
  ParamsForm.meLocal.StoreDefs := true;
  ParamsForm.meLocal.FieldDefs.Add('dumb', TFieldType.ftString, 20, false);

  ParamsForm.ClearControls;

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select * from report_params where report_id = :report_id order by param_order');
  dm.qrAux.ParamByName('report_id').Value := dm.meData.FieldByName('rowid').AsInteger;
  dm.qrAux.Open;

  sumHeight := 0;

  while not dm.qrAux.Eof do
  begin

    pl := TPanel.Create(ParamsForm);
    pl.Height := 32;
    pl.AlignWithMargins := true;
    pl.Parent := ParamsForm.plWork;
    pl.Top := 200;
    pl.Align := alTop;
    pl.BorderStyle := bsNone;
    pl.BevelKind := bkNone;
    pl.BevelInner := bvNone;
    pl.BevelOuter := bvNone;
    pl.Show;

    sumHeight := sumHeight + 45;

    l := TLabel.Create(ParamsForm);
    l.Caption := dm.qrAux.FieldByName('param_title').AsString;
    l.AutoSize := false;
    l.AlignWithMargins := true;
    l.Constraints.MaxWidth := 300;
    l.Constraints.MinWidth := 300;
    l.Show;
    l.Parent := pl;
    l.Align := alLeft;

    c := MakeControl(
        dm.qrAux.FieldByName('param_name').AsString,
        dm.qrAux.FieldByName('param_type').AsInteger,
        dm.qrAux.FieldByName('param_field_length').AsInteger,
        dm.qrAux.FieldByName('param_decimal_length').AsInteger,
        dm.qrAux
        );

    c.Parent := pl;
    c.AlignWithMargins := true;
    c.Align := alClient;

    if dm.qrAux.FieldByName('param_type').AsInteger = 3 then
      ParamsForm.meLocal.FieldDefs.Add(dm.qrAux.FieldByName('param_name').AsString, TFieldType.ftDateTime, 0, false)
    else
      ParamsForm.meLocal.FieldDefs.Add(dm.qrAux.FieldByName('param_name').AsString, TFieldType.ftVariant, 200, false);

    dm.qrAux.Next;

  end;

  ParamsForm.meLocal.CreateDataSet;
  ParamsForm.meLocal.Open;
  ParamsForm.meLocal.Append;

  ParamsForm.Height := sumHeight + 140;

  ParamsForm.Caption := dm.meData.FieldByName('report_name').AsString;

end;

procedure TFormMain.MakeExcelReport;
var grp1, grp2: string; sl: TStringList; rf: TEXLReportField; i: integer;
begin

  sl := TStringList.Create;
  sl.Delimiter := ',';
  sl.StrictDelimiter := true;
  sl.DelimitedText := dm.meData.FieldByName('group_fields').AsString;

  grp1 := '';
  grp2 := '';

  if sl.Count>0 then grp1 := sl[0];
  if sl.Count>1 then grp2 := sl[1];

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select * from report_templates where report_id = :report_id');
  dm.qrAux.ParamByName('report_id').Value := dm.meData.FieldByName('rowid').AsInteger;
  dm.qrAux.Open;

  if trim(dm.meData.FieldByName('report_template').AsString) = '' then
  begin
    ExportExcel(dpReports, dm.meData.FieldByName('report_name').AsString);
    exit;
  end;

  exReport.Dictionary.Clear;
  rf := exReport.Dictionary.Add;
  rf.FieldName := 'print_title';
  rf.ValueAsString := dm.meData.FieldByName('report_name').AsString;


  for i := 1 to ParamsForm.meLocal.FieldCount-1 do
  begin

    rf := exReport.Dictionary.Add;
    rf.FieldName := ParamsForm.meLocal.Fields[i].FieldName;
    rf.ValueAsString := VarToStr(ParamsForm.meLocal.Fields[i].Value);

  end;

  meReport.DisableControls;

  exReport.Template := ExtractFilePath(Application.ExeName) +'/templates/'+trim(dm.meData.FieldByName('report_template').AsString);
  exReport.Show('', false, grp1, grp2);

  meReport.EnableControls;
  Screen.Cursor := crDefault;

end;

procedure TFormMain.N2Click(Sender: TObject);
begin
  FormReports.Init;
  FormReports.ShowModal;
  dm.meData.Close;
  dm.meData.Open;
end;

function TFormMain.MakeControl(field_name: string; data_type: integer; data_length: integer; decimal_length: integer; dsParam: TDataSet): TControl;
var c: TControl; ss: TAdoLookUpSqlSet;
begin
  c:= nil;
  if data_type = 1 then
  begin
    c := TDBEditEh.Create(ParamsForm);
    TDbEditEh(c).DataSource := ParamsForm.dsLocal;
    TDbEditEh(c).DataField := field_name;
  end;
  if data_type = 2 then
  begin
    c := TDBNumberEditEh.Create(ParamsForm);
    with TDBNumberEditEh(c) do
    begin
      DataSource := ParamsForm.dsLocal;
      DataField := field_name;
      DecimalPlaces := decimal_length;
    end;
  end;
  if data_type = 3 then
  begin
    c := TDBDateTimeEditEh.Create(ParamsForm);
    TDBDateTimeEditEh(c).DataSource := ParamsForm.dsLocal;
    TDBDateTimeEditEh(c).DataField := field_name;
  end;
  if data_type = 4 then
  begin
    c := TDBSQLLookUp.Create(ParamsForm);
    TDBSQLLookUp(c).DataSource := ParamsForm.dsLocal;
    TDBSQLLookUp(c).DataField := field_name;
    ss := TAdoLookUpSqlSet.Create(c);

    ss.TmplSql.Text :=
    StringReplace(
       dsParam.FieldByName('param_sql').AsString,
       '<wherefilter>', 'where '+dsParam.FieldByName('param_display').AsString+' like ''%@pattern%''', [rfReplaceAll]);

    ss.TmplSql.Text :=
    StringReplace(
       ss.TmplSql.Text,
       '<andfilter>', 'and '+dsParam.FieldByName('param_display').AsString+' like ''%@pattern%''', [rfReplaceAll]);

    ss.DownSql.Text :=
    StringReplace(
       dsParam.FieldByName('param_sql').AsString,
       '<wherefilter>', '', [rfReplaceAll]);

    ss.DownSql.Text :=
    StringReplace(
       ss.DownSql.Text,
       '<andfilter>', '', [rfReplaceAll]);

    ss.InitSql.Text :=
    StringReplace(
       dsParam.FieldByName('param_sql').AsString,
       '<wherefilter>', 'where '+dsParam.FieldByName('param_key').AsString+' = @id', [rfReplaceAll]);

    ss.InitSql.Text :=
    StringReplace(
       ss.InitSql.Text,
       '<andfilter>', 'and '+dsParam.FieldByName('param_key').AsString+' = @id', [rfReplaceAll]);

    ss.KeyName := dsParam.FieldByName('param_key').AsString;
    ss.DisplayName := dsParam.FieldByName('param_display').AsString;

    ss.Connection := dm.connReport;

    TDBSQLLookUp(c).AutoSelect := false;
    TDBSQLLookUp(c).sqlSetLinked := ss;
    TDBSQLLookUp(c).SqlSet := ss;
    TDBSQLLookUp(c).DataField := field_name;

  end;
  result := c;
end;


procedure TFormMain.AssignParameters(dsParams: TDataSet);
var i: integer; param_name: string;
begin

  // подготовительная команда

  for i := 0 to dsParams.FieldCount-1 do
  begin

    param_name := dsParams.Fields[i].FieldName;

    if (param_name<>'dumb') then
    begin
       if cmdPrepareReport.Parameters.FindParam(param_name) = nil then
       continue;

       if dsParams.FieldByName(param_name).DataType = ftDateTime then
        cmdPrepareReport.Parameters.ParamByName(param_name).Value := dsParams.FieldByName(param_name).AsDateTime
       else
        cmdPrepareReport.Parameters.ParamByName(param_name).Value := dsParams.FieldByName(param_name).Value;

    end;
  end;

  // запрос отчета

  for i := 0 to dsParams.FieldCount-1 do
  begin

    param_name := dsParams.Fields[i].FieldName;

    if (param_name<>'dumb') then
    begin

       if qrReport.Parameters.FindParam(param_name) = nil then
       continue;

       if dsParams.FieldByName(param_name).DataType = ftDateTime then
        qrReport.Parameters.ParamByName(param_name).Value := dsParams.FieldByName(param_name).AsDateTime
       else
        qrReport.Parameters.ParamByName(param_name).Value := dsParams.FieldByName(param_name).Value;

    end;
  end;

end;


function TFormMain.GotParams: boolean;
begin

  result := false;

  if ParamsForm.meLocal.FieldCount<2 then
    result := true
  else begin
    ParamsForm.ShowModal;
    ParamsForm.meLocal.Post;
    if ParamsForm.ModalResult = mrOk then
      result := true;
  end;

end;



end.
