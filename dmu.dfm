﻿object dm: Tdm
  OldCreateOrder = False
  Height = 588
  Width = 846
  object connSettings: TFDConnection
    Params.Strings = (
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 56
    Top = 32
  end
  object connReport: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=111;Persist Security Info=True;User ' +
      'ID=postgres;Data Source=PostgreSQL30'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 58
    Top = 96
  end
  object tblConnection: TFDTable
    Connection = connSettings
    UpdateOptions.UpdateTableName = 'connectionparams'
    TableName = 'connectionparams'
    Left = 128
    Top = 31
  end
  object qrAux: TFDQuery
    Connection = connSettings
    Left = 131
    Top = 96
  end
  object drvData: TFDTableAdapter
    SelectCommand = SelectCmd
    InsertCommand = InsertCmd
    UpdateCommand = UpdateCmd
    DeleteCommand = DeleteCmd
    FetchRowCommand = FetchRowCmd
    Left = 48
    Top = 176
  end
  object meData: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = drvData
    Left = 112
    Top = 176
  end
  object SelectCmd: TFDCommand
    Connection = connSettings
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'select r.rowid, r.* from reports r order by report_name')
    Left = 48
    Top = 247
  end
  object DeleteCmd: TFDCommand
    Connection = connSettings
    Left = 104
    Top = 247
  end
  object UpdateCmd: TFDCommand
    Connection = connSettings
    Left = 160
    Top = 247
  end
  object InsertCmd: TFDCommand
    Connection = connSettings
    Left = 216
    Top = 247
  end
  object FetchRowCmd: TFDCommand
    Connection = connSettings
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 272
    Top = 247
  end
end
