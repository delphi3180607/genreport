﻿inherited FormCatalogs: TFormCatalogs
  Caption = #1050#1072#1090#1072#1083#1086#1075#1080
  ClientWidth = 554
  ExplicitWidth = 570
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 554
    ExplicitWidth = 554
    inherited btnOk: TButton
      Left = 319
      ExplicitLeft = 319
    end
    inherited btnCancel: TButton
      Left = 438
      ExplicitLeft = 438
    end
  end
  inherited plAll: TPanel
    Width = 554
    ExplicitWidth = 554
    inherited plTop: TPanel
      Width = 554
      ExplicitWidth = 554
      inherited plCount: TPanel
        Left = 360
        ExplicitLeft = 360
      end
    end
    inherited dgData: TDBGridEh
      Width = 554
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'catalog_name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 413
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 34
        Height = 13
      end
    end
  end
end
