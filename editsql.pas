﻿unit editsql;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, Vcl.ExtCtrls, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.Buttons, PngSpeedButton, SynEditMiscClasses, SynEditSearch,
  SynEditHighlighter, SynHighlighterSQL, SynEdit, SynMemo, Data.DB,
  Data.Win.ADODB, Vcl.StdCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFormEditSql = class(TFormEdit)
    meSQL: TSynMemo;
    synSQL: TSynSQLSyn;
    synSearch: TSynEditSearch;
    dsTest: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditSql: TFormEditSql;

implementation

{$R *.dfm}

end.
