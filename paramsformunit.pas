﻿unit ParamsFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, MemTableDataEh, MemTableEh, Vcl.Mask, Vcl.DBCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TParamsForm = class(TFormEdit)
    meLocal: TMemTableEh;
    plWork: TPanel;
  private
    { Private declarations }
  public
    procedure ClearControls;
  end;

var
  ParamsForm: TParamsForm;

implementation

{$R *.dfm}

procedure TParamsForm.ClearControls;
begin

  while plWork.ControlCount>0 do
  begin
    plWork.Controls[0].Free;
  end;

end;


end.
