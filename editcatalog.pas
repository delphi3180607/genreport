﻿unit editcatalog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB, DBGridEh, DBLookupEh, Vcl.Buttons,
  DBSQLLookUp, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFormEditCatalog = class(TFormEdit)
    Label1: TLabel;
    edFolderName: TDBEditEh;
    Label2: TLabel;
    sbParentFolders: TSpeedButton;
    leParentCatalog: TDBSQLLookUp;
    ssParentCatalog: TADOLookUpSqlSet;
  private
    Fsystem_section: string;
    procedure SetSystemSection(value: string);
  public
    property system_section: string read Fsystem_section write SetSystemSection;
  end;

var
  FormEditCatalog: TFormEditCatalog;

implementation

{$R *.dfm}

procedure TFormEditCatalog.SetSystemSection(value: string);
begin
 Fsystem_section := value;
 ssParentCatalog.Parameters.SetPairValue('folder_section',value);
end;


end.
